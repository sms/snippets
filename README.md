# statistics-snippets

These are the code snippets as used in the course spatial modeling and statistics from the Wageningen University. 
The code in the snippets has been written by Sytze de Bruin.