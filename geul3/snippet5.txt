# where is probability of exeeding ADI > 0.05?
st_MCprob$p95 <- factor(ifelse(st_MCprob$prob > 0.05, 1, 0), levels=c(0,1),
                        labels=c("safe", "hazard"))
ggplot() +
  geom_stars(data = st_MCprob["p95"]) +
  coord_fixed() +
  scale_fill_manual(values=c("green", "red")) +
  ggtitle("Safe/hazard at 95% level") +
  xlim(190200,191300) + ylim(314300,315600)
