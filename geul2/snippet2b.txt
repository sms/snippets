p1 <- ggplot() +
  geom_stars(data=geul.krig["var1.pred"]) +
  scale_fill_gradient(low = "dark blue", high = "yellow", limits=c(0,650)) +
  ggtitle("Pb") +
  xlim(190200,191300) +
  ylim(314300,315600) +
  theme(legend.title = element_blank())

p2 <- ggplot() +
  geom_stars(data=geul.krig["var1.sd"], show.legend = FALSE) +
  ggtitle("sd(Pb)") +
  scale_fill_gradient(low = "dark blue", high = "yellow", limits=c(0,650)) +
  xlim(190200,191300) +
  ylim(314300,315600)

# place the two plots alongside (using the patchwork library)
x11(width=8,height=4) # Open separate graphics window
p1 + coord_fixed() + p2 + coord_fixed()