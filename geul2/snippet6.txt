# Plot a few simulations
plot(geul.logsim[,,,1:6], xlim=c(190200,191300), 
       ylim=c(314300,315600), col = sp::bpy.colors())

# Alternative with ggplot
ggplot() +
  geom_stars(data=geul.logsim[,,,1:6]) +
  facet_wrap(vars(sample)) +
  scale_fill_gradient(low = "dark blue", high = "yellow") +
  xlim(190200,191300) +
  ylim(314300,315600) +
  coord_fixed() +
  theme(axis.title=element_blank(),
        axis.text=element_blank()) +
  labs(fill='Pb [ppm]')