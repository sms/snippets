# Study anisotropy: variogram map
vgpb_map  <- variogram(gpb, cutoff=420, width=70, map=TRUE)
plot(vgpb_map, col.regions = sp::bpy.colors(), main="pb~1")

# In case the above didn't work
sp::spplot(vgpb_map[[1]]["var1"], scales=list(draw=T), 
           col.regions = sp::bpy.colors())

# Check anisotropies: alpha and tol.hor
vgpb_dir  <- variogram(pb~1, data = geul,alpha=c(60,150), tol.hor=45, 
                       cutoff = 800, width=80)
plot(vgpb_dir,ylim=c(0,40000), main="directional variogram")

# Fit variogram to the longest axis
vgpb_150  <- variogram(pb~1, data = geul,alpha=150, tol.hor=45, cutoff = 800, 
                       width=80)
vgmpb_150 <- vgm(25000, "Exp", 200)
vgmpb_150 <- fit.variogram(vgpb_150, vgmpb_150)
vgmpb_150
plot(vgpb_150, vgmpb_150)

# Fit opposite direction to the shortest axis
vgpb_60  <- variogram(pb~1, data = geul,alpha=60, tol.hor=45, cutoff = 800, 
                      width=80)
plot(vgpb_60)
vgmpb_60 <- vgm(30487, "Exp", 50)
vgmpb_60 <- fit.variogram(vgpb_60, vgmpb_60, fit.sills = FALSE)
plot(vgpb_60, vgmpb_60)
vgmpb_60

# Combine both into a single model
ani_ratio =vgmpb_60$range/vgmpb_150$range
vgmpb_dir <- vgm(30487, "Exp", 249, anis=c(150, ani_ratio))
vgmpb_dir <- fit.variogram(vgpb_dir, vgmpb_dir)
vgmpb_dir
plot(vgpb_dir, vgmpb_dir)

# Create 2D image of variogram model values
vgm_value <- function(dist, dir_x, dir_y) 
  variogramLine(vgmpb_dir, dir=c(dir_x, dir_y, 0), min=1, 
                dist_vector=dist)$gamma

grid <- as.data.frame(vgpb_map[[1]]@coords)
grid$dist <- sqrt(grid$dx^2 + grid$dy^2)
grid$dirx <- grid$dx/grid$dist
grid$diry <- grid$dy/grid$dist
vgpb_map$map$model <- mapply(vgm_value, grid$dist, grid$dirx, grid$diry)
vgpb_map$map$model[which(grid$dist==0)] <- 0
plot(vgpb_map, col.regions=sp::bpy.colors())
