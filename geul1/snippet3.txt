vgmpb2 <- vgm(psill = 14000, range = 150, model = "Sph", 
              add.to = vgm(psill = 16000, range = 500, model = "Sph"))
vgmpb2 <- fit.variogram(vgpb, vgmpb2)
plot(vgpb, vgmpb2)
vgmpb2
attr(vgmpb2, "SSErr")
